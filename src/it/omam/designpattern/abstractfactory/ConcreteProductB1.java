package it.omam.designpattern.abstractfactory;

public class ConcreteProductB1 implements AbstractProductB
{
    @Override
    public void print()
    {
        System.out.println("ConcreteProductB1");
    }
}
