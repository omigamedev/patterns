package it.omam.designpattern.builder;

public class Main
{
    public static void main(String[] args)
    {
        Weiter w = new Weiter();
        
        System.out.println("building Pizza Margherita");
        PizzaMargheritaBuilder m = new PizzaMargheritaBuilder();
        w.makePizza(m);
        m.getPizzaMargherita().eat();
        
        System.out.println("building Pizza Peperoni");
        PizzaPeperoniBuilder p = new PizzaPeperoniBuilder();
        w.makePizza(p);
        p.getPizzaMargherita().eat(); 

    }
}
