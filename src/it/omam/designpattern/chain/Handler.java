package it.omam.designpattern.chain;

public class Handler
{
    private Handler _nextHandler = null;

    public Handler(Handler nextHandler)
    {
        _nextHandler = nextHandler;
    }

    public void HandleEvent(int type)
    {
        switch(type)
        {
        case Events.EVENT_NONE:
            System.out.println("Nothign to do");
            break;
        case Events.EVENT_INFO:
            System.out.println("Some info...");
            break;
        default:
            if(_nextHandler != null)
            {
                System.out.println(this.getClass().getSimpleName() + " forwarding event");
                _nextHandler.HandleEvent(type);
            }
            else
            {
                System.out.println("Reached the end of the chain");
            }
            break;
        }
    }
}
