package it.omam.designpattern.abstractfactory;

public class ConcreteProductA1 implements AbstractProductA
{
    @Override
    public void print()
    {
        System.out.println("ConcreteProductA1");
    }
}
