package it.omam.designpattern.adapter;

public interface AbstractPrinter
{
    public void print();
}
