package it.omam.designpattern.builder;

public class PizzaMargheritaBuilder extends PizzaBuilder
{
    @Override
    public void buildPasta()
    {
        System.out.println("buildPasta");
    }

    @Override
    public void buildCheese()
    {
        System.out.println("buildCheese");
    }

    @Override
    public void buildSauce()
    {
        System.out.println("buildSauce");
    }

    public PizzaMargherita getPizzaMargherita()
    {
        return new PizzaMargherita();
    }
}
