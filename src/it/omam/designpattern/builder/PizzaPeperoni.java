package it.omam.designpattern.builder;

public class PizzaPeperoni implements Pizza
{
    @Override
    public void eat()
    {
        System.out.println("eating Pizza Peperoni");
    }
}
