package it.omam.designpattern.bridge;

public abstract class Abstraction
{
    private AbstractImplementation implementation;

    public Abstraction()
    {
        implementation = new ImplementationB(); // maybe implemented using a Factory Method
    }
    
    public void printOperation()
    {
        implementation.printOperation();
    }

    public abstract void firstOperation();

    public abstract void secondOperation();
}
