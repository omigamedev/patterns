package it.omam.designpattern.chain;

public class Application extends Handler
{
    public Application(Handler nextHandler)
    {
        super(nextHandler);
    }

    @Override
    public void HandleEvent(int type)
    {
        if(type == Events.EVENT_APPLICATION_INFO)
        {
            System.out.println("Application info");
        }
        else
        {
            super.HandleEvent(type);
        }
    }
}
