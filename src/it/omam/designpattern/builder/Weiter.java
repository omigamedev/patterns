package it.omam.designpattern.builder;

public class Weiter
{
    public PizzaBuilder makePizza(PizzaBuilder builder)
    {
        builder.buildPasta();
        builder.buildSauce();
        builder.buildCheese();
        builder.buildPeperoni();
        
        return builder;
    }
}
