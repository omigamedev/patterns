package it.omam.designpattern.abstractfactory;

public interface AbstractProductA
{
    public void print();
}
