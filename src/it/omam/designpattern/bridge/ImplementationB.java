package it.omam.designpattern.bridge;

public class ImplementationB extends AbstractImplementation
{
    @Override
    public void printOperation()
    {
        System.out.println("ImplementationB");
    }
}
