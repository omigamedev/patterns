package it.omam.designpattern.abstractfactory;

public class ConcreteProductB2 implements AbstractProductB
{
    @Override
    public void print()
    {
        System.out.println("ConcreteProductB2");
    }
}
