package it.omam.designpattern.builder;

public abstract class PizzaBuilder
{
    public void buildPasta()
    {
        System.out.println("ignore buildPasta");
    }
    
    public void buildCheese()
    {
        System.out.println("ignore buildSauce");
    }
    
    public void buildSauce()
    {
        System.out.println("ignore buildSauce");
    }
    
    public void buildPeperoni()
    {
        System.out.println("ignore buildPeperoni");
    }
}
