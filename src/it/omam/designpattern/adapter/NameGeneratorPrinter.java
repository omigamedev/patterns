package it.omam.designpattern.adapter;

public class NameGeneratorPrinter implements AbstractPrinter
{
    private NameGenerator ng = null;

    public NameGeneratorPrinter(NameGenerator ng)
    {
        super();
        setNameGenerator(ng);
    }
    
    public void setNameGenerator(NameGenerator ng)
    {
        this.ng = ng;
    }

    @Override
    public void print()
    {
        System.out.println(ng != null ? ng.generate() : "NameGenerator not defined");
    }
}
