package it.omam.designpattern.bridge;

public class Main
{
    public static void main(String[] args)
    {
        RefinedAbstraction refined = new RefinedAbstraction();
        refined.firstOperation();
        refined.secondOperation();
    }
}
