package it.omam.designpattern.adapter;

public class NameGenerator
{
    private String[] first = {"Paolo", "Gino", "Fringuello", "Ernesto"};
    private String[] second = {"Malefico", "il Viscido", "Rompighiaccio", "Raperonzolo"};

    public String generate()
    {
        return first[(int)(Math.random() * first.length)] + " " +
            second[(int)(Math.random() * second.length)];
    }
}
