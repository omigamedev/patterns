package it.omam.designpattern.chain;

public abstract class Events
{
    public static final int EVENT_NONE = 0;
    public static final int EVENT_INFO = 1;
    public static final int EVENT_APPLICATION_INFO = 2;
    public static final int EVENT_VIEW_INFO = 3;
    public static final int EVENT_BUTTON_INFO = 4;
}
