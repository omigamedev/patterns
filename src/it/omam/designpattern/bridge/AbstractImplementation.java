package it.omam.designpattern.bridge;

public abstract class AbstractImplementation
{
    public abstract void printOperation();
}
