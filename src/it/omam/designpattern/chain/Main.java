package it.omam.designpattern.chain;

public class Main
{
    public static void main(String[] args)
    {
        Application app = new Application(null);
        View view = new View(app);
        Button button = new Button(view);
        button.HandleEvent(Events.EVENT_APPLICATION_INFO);
        button.HandleEvent(100);
    }
}
