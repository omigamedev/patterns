package it.omam.designpattern.adapter;

import java.util.ArrayList;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        NameGenerator ng = new NameGenerator();
        
        List<AbstractPrinter> batch = new ArrayList<AbstractPrinter>();
        batch.add(new NumberPrinter());
        batch.add(new NameGeneratorPrinter(ng));
        
        for(AbstractPrinter printer : batch)
        {
            printer.print();
        }
        
        System.out.println("end of the pattern");
    }
}
