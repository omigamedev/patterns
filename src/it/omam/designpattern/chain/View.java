package it.omam.designpattern.chain;

public class View extends Handler
{
    public View(Handler nextHandler)
    {
        super(nextHandler);
    }

    @Override
    public void HandleEvent(int type)
    {
        if(type == Events.EVENT_VIEW_INFO)
        {
            System.out.println("View info");
        }
        else
        {
            super.HandleEvent(type);
        }
    }
}
