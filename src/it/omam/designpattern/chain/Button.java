package it.omam.designpattern.chain;

public class Button extends Handler
{
    public Button(Handler nextHandler)
    {
        super(nextHandler);
    }

    @Override
    public void HandleEvent(int type)
    {
        if(type == Events.EVENT_BUTTON_INFO)
        {
            System.out.println("Button info");
        }
        else
        {
            super.HandleEvent(type);
        }
    }
}
