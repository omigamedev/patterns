package it.omam.designpattern.bridge;


public class RefinedAbstraction extends Abstraction
{
    @Override
    public void firstOperation()
    {
        printOperation();
    }

    @Override
    public void secondOperation()
    {
        for(int i = 0; i < 3; i++)
        {
            printOperation();
        }
    }
}
