package it.omam.designpattern.bridge;

public class ImplementationA extends AbstractImplementation
{
    @Override
    public void printOperation()
    {
        System.out.println("ImplementationA");
    }
}
