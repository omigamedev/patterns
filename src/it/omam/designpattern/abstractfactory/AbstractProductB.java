package it.omam.designpattern.abstractfactory;

public interface AbstractProductB
{
    public void print();
}
