package it.omam.designpattern.builder;

public class PizzaMargherita implements Pizza
{
    @Override
    public void eat()
    {
        System.out.println("eating Pizza Margherita");
    }
}
