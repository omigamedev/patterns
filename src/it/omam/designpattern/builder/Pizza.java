package it.omam.designpattern.builder;

public interface Pizza
{
    public void eat();
}
