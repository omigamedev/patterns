package it.omam.designpattern.abstractfactory;

public abstract class AbstractFactory
{
    private static AbstractFactory singleton = null;
    
    public static AbstractFactory getFactory()
    {
        return singleton != null ? singleton : (singleton = new ConcreteFactory1());
    }
    
    public abstract AbstractProductA createProductA();
    public abstract AbstractProductB createProductB();
}
