package it.omam.designpattern.adapter;

public class NumberPrinter implements AbstractPrinter
{
    @Override
    public void print()
    {
        System.out.println("Random number: " + (int)(Math.random() * 1000));
    }
}
