package it.omam.designpattern.builder;

public class PizzaPeperoniBuilder extends PizzaBuilder
{
    @Override
    public void buildPasta()
    {
        System.out.println("buildPasta");
    }

    @Override
    public void buildCheese()
    {
        System.out.println("buildCheese");
    }

    @Override
    public void buildSauce()
    {
        System.out.println("buildSauce");
    }

    @Override
    public void buildPeperoni()
    {
        System.out.println("buildPeperoni");
    }

    public PizzaPeperoni getPizzaMargherita()
    {
        return new PizzaPeperoni();
    }
}
