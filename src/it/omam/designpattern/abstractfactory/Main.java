package it.omam.designpattern.abstractfactory;

public class Main
{
    public static void main(String[] args)
    {
        AbstractFactory factory = AbstractFactory.getFactory();
        AbstractProductA pA = factory.createProductA();
        AbstractProductB pB = factory.createProductB();
        
        pA.print();
        pB.print();
        
        System.out.println("end of the pattern");
    }
}
