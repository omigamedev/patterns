package it.omam.designpattern.abstractfactory;

public class ConcreteProductA2 implements AbstractProductA
{
    @Override
    public void print()
    {
        System.out.println("ConcreteProductA2");
    }
}
